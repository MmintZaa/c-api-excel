﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApplicationTestAPI.Data;
using WebApplicationTestAPI.Models;

namespace WebApplicationTestAPI.Controllers
{



    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
       

        [HttpGet]
        //IEnumerable<Usermodel>
        public IEnumerable<Usermodel> Get()
        {
            List<Usermodel> list = new List<Usermodel>();
            list.Add(new Usermodel
            {
                id = 1,
                name = "user1",
                age = 10
            });
            list.Add(new Usermodel
            {
                id = 2,
                name = "user2",
                age = 20
            });

          
            return list;

        }




        [HttpGet("{id}")]
        public Usermodel Get(int id)
        {
            return new Usermodel
            {
                id = id,
                name = "user1",
                age = 20
            };
        }

        [HttpDelete("{id}")]
        public Usermodel Delete(Usermodel id)
        {
            id.name = "Created";
            return id;

        
    }

    [HttpPost]
    public Usermodel Create (Usermodel data)
    {
        data.name += "Created";
        return data;
    }

    


    [HttpPut]
    public Usermodel Update(Usermodel data)
    {
        data.name += "Updateed";
        return data;
    }


   
}


}
