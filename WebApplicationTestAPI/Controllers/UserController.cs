﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApplicationTestAPI.Data;
using WebApplicationTestAPI.Models;

namespace ApiExcel.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private my_database _dbContext;
        public UserController(my_database dbContext)
        {
            _dbContext = dbContext;
        }
        [HttpGet("GetUser")]

        public IActionResult Get()
        {
            /* var u = GetUsers();

             return Ok(u);*/

            try
            {
                var u = _dbContext.db_tblusers.ToList();
                if (u.Count ==0)
                {
                    return StatusCode(404, "No user found");
                }

            return Ok(u);
            }catch(Exception )
            {
                return StatusCode(500, "An error has occurred");
            }
           
        }

        [HttpPost("CreateUser")]

        public IActionResult Create([FromBody] Usermodel request)
        {
            db_tbluser u = new db_tbluser();       
            u.name = request.name;
            u.age = request.age;

            try
            {
                _dbContext.db_tblusers.Add(u);
                _dbContext.SaveChanges();
            }
            catch (Exception)
            {
                return StatusCode(500, "An error has occurred");
            }

        
            return Ok(u);

        }

        [HttpPut("UpdateUser")]

        public IActionResult Update([FromBody] Usermodel request)
        {


            try
            {
                var u = _dbContext.db_tblusers.FirstOrDefault(x => x.id == request.id);
                if (u == null)
                {
                    return StatusCode(404, "No user found");
                }
                u.name = request.name;
                u.age = request.age;
                _dbContext.Entry(u).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _dbContext.SaveChanges();
                return Ok(u);
            }
            catch (Exception)
            {
                return StatusCode(500, "An error has occurred");
            }

        }

        [HttpDelete("DeleteUser/{id}")]

        public IActionResult Delete([FromRoute] int id)
        {


            try
            {
                var u = _dbContext.db_tblusers.FirstOrDefault(x => x.id == id);
                if (u == null)
                {
                    return StatusCode(404, "No user found");
                }
               
                _dbContext.Entry(u).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _dbContext.SaveChanges();
                return Ok(u);
            }
            catch (Exception)
            {
                return StatusCode(500, "An error has occurred");
            }

        }

    }


       /* private   List<Usermodel> GetUsers(){
            return new List<Usermodel> {
                new Usermodel { name="mint", age=20 },
            };
            }*/
    }

