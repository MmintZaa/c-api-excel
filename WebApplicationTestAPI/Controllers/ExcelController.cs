﻿using System;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using WebApplicationTestAPI.Data;

namespace ApiExcel.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExcelController : ControllerBase
    {
        private my_database _dbContext;
        public ExcelController(my_database dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        [Route("export")]
        public IActionResult export()
        {
            try
            {
                var data = _dbContext.db_tblexcel.ToList();

             
                using (var workbook = new XLWorkbook())
                {
                    IXLWorksheet sheet = workbook.Worksheets.Add();
                    sheet.Cell(1, 1).Value = "no";
                    sheet.Cell(1, 2).Value = "task";
                    sheet.Cell(1, 3).Value = "mandayTask";
                    sheet.Cell(1, 4).Value = "study";
                    sheet.Cell(1, 5).Value = "mandayStudy";
                    sheet.Cell(1, 6).Value = "sumDay";
                    sheet.Cell(1, 7).Value = "leaveWork";
                    sheet.Cell(1, 8).Value = "sum";
              

                    var row = 1;
                    foreach (var item in data)
                    {
                        sheet.Cell(row + 1, 1).Value = row;
                        sheet.Cell(row + 1, 2).Value = item.task;
                        sheet.Cell(row + 1, 3).Value = item.mandayTask;
                        sheet.Cell(row + 1, 4).Value = item.study;
                        sheet.Cell(row + 1, 5).Value = item.mandayStudy;
                        sheet.Cell(row + 1, 6).Value = item.sumDay;
                        sheet.Cell(row + 1, 7).Value = item.leaveWork;
                        sheet.Cell(row + 1, 8).Value = item.sum;                 
                       
                        row++;
                    }

                    using (var stream = new MemoryStream())
                    {
                        workbook.SaveAs(stream);
                        if (stream != null)
                        {
                            string excelName = $"ExcelList-{DateTime.Now.ToString()}.xlsx";
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
                       
                        }
                        else
                        {
                            return NotFound();
                        }
                        
                    }

                }
            }
            catch (Exception ex)
            {
                return (IActionResult)ex;
            }
        }

    }

    }

