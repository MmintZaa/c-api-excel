﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplicationTestAPI.Data
{
    public class db_tbluser
    {
        [Key]
        public int id { get; set; }
        public String name { get; set; }
        public int age { get; set; }
    }
}
