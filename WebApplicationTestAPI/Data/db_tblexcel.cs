﻿using System;
using System.ComponentModel.DataAnnotations;


namespace ApiExcel.Data
{
    public class db_tblexcel
    {

        [Key]
        public int no { get; set; }
        public String task { get; set; }
        public int mandayTask { get; set; }
        public String study { get; set; }
        public int mandayStudy { get; set; }
        public int sumDay { get; set; }
        public int leaveWork { get; set; }
        public int sum { get; set; }
    }
}
