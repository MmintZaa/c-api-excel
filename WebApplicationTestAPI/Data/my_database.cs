﻿using System;
using ApiExcel.Data;
using Microsoft.EntityFrameworkCore;

namespace WebApplicationTestAPI.Data
{
    public class my_database : DbContext
    {
        public my_database(DbContextOptions<my_database> options) : base(options)
        {

        }

        public DbSet<db_tbluser> db_tblusers { get; set; }
        public DbSet<db_tblexcel> db_tblexcel { get; set; }
    }
}
