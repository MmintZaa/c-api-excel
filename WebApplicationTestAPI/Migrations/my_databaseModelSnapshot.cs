﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebApplicationTestAPI.Data;

namespace ApiExcel.Migrations
{
    [DbContext(typeof(my_database))]
    partial class my_databaseModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.23")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("ApiExcel.Data.db_tblexcel", b =>
                {
                    b.Property<int>("no")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("leaveWork")
                        .HasColumnType("int");

                    b.Property<int>("mandayStudy")
                        .HasColumnType("int");

                    b.Property<int>("mandayTask")
                        .HasColumnType("int");

                    b.Property<string>("study")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("sum")
                        .HasColumnType("int");

                    b.Property<int>("sumDay")
                        .HasColumnType("int");

                    b.Property<string>("task")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("no");

                    b.ToTable("db_tblexcel");
                });

            modelBuilder.Entity("WebApplicationTestAPI.Data.db_tbluser", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("age")
                        .HasColumnType("int");

                    b.Property<string>("name")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("id");

                    b.ToTable("db_tblusers");
                });
#pragma warning restore 612, 618
        }
    }
}
