﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiExcel.Migrations
{
    public partial class _42 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "db_tblexcel",
                columns: table => new
                {
                    no = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    task = table.Column<string>(nullable: true),
                    mandayTask = table.Column<int>(nullable: false),
                    study = table.Column<string>(nullable: true),
                    mandayStudy = table.Column<int>(nullable: false),
                    sumDay = table.Column<int>(nullable: false),
                    leaveWork = table.Column<int>(nullable: false),
                    sum = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_db_tblexcel", x => x.no);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "db_tblexcel");
        }
    }
}
